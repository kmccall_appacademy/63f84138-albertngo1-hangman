
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @guesser = players[:guesser]
    @referee = players[:referee]
    @maximum_number_guesses = 10
  end

  def setup
    secret_word = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word)
    @board = [nil] * secret_word
  end

  def take_turn
    guess = @guesser.guess(@board)
    check = @referee.check_guess(guess)
    update_board(guess, check)
    @guesser.handle_response(guess, check)
    @maximum_number_guesses -= 1
  end

  def update_board(letter, indices)
    indices.each { |i| @board[i] = letter }
  end

  def play
    setup

    until @maximum_number_guesses == 0
      p @board
      puts "You have #{@maximum_number_guesses}/10 guesses remaining"
      take_turn
      break if won?
    end
    if won?
      puts "Congratulations, you solved the word, #{@referee.require_secret}!"
    else
      puts "You did not guess the word."
      puts "The word is #{@referee.require_secret}"
    end
  end

  def won?
    @board.all?
  end
end

class HumanPlayer

  def register_secret_length(secret_length)
    puts "The length of the word is #{secret_length} letters long"
  end

  def guess(board)
    puts "What letter would you like to guess?"
    puts "Secret word: #{board}"
    gets.chomp
  end

  def handle_response(guess, response)
    puts "Found the letter #{guess} at positions #{response}"
  end

  def pick_secret_word
    puts "Think of a word for this game. How many letters long is this word?"

    gets.chomp.to_i
  end

  def check_guess(guess)
    puts "Which indices contains the letter #{guess}? Press enter if
    the word does not contain #{guess}"
    gets.chomp.split(',').map { |i| Integer(i)}
  end

  def require_secret
    puts "What was your word?"
    gets.chomp
  end
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :candidate_words

  def self.computer_dict_play(dict_file)
    ComputerPlayer.new(File.readlines(dict_file).map(&:chomp))
  end

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.shuffle.first

    @secret_word.length
  end

  def check_guess(letter)
    right_letters = []
    @secret_word.chars.each_with_index do |char, idx|
      right_letters << idx if char == letter
    end
    right_letters
  end
  def require_secret
    @secret_word
  end

  def register_secret_length(guess_length)
    @candidate_words = dictionary.select { |word| word.length == guess_length}
  end

  def guess(board)
    freq_table = freq_table(board)

    most_frequent_letters = freq_table.sort_by {|k, v| v}
    #most_frequent_letters[-1][0]
    letter, _ = most_frequent_letters.last
    letter
  end

  def handle_response(guess, response_indices)
    @candidate_words.reject! do |word|
      should_delete = false
      word.split('').each_with_index do |char, idx|
        if char == guess && !response_indices.include?(idx)
          should_delete = true
          break
        elsif char != guess && response_indices.include?(idx)
          should_delete = true
          break
        end
      end
      should_delete
    end
  end

  private

  def freq_table(board)
    freq_table = Hash.new(0)

    @candidate_words.each do |word|
      board.each_with_index do |letter, idx|
        freq_table[word[idx]] += 1 if letter.nil?
      end
    end
    freq_table
  end
end

if __FILE__ == $PROGRAM_NAME
  puts "Is the guesser a computer? (Yes/No)"
  if gets.chomp.downcase == "yes"
    guesser = ComputerPlayer.computer_dict_play('dictionary.txt')
  else
    guesser = HumanPlayer.new
  end

  puts "Is the referee a computer? (Yes/No)"
  if gets.chomp.downcase == "yes"
    referee = ComputerPlayer.computer_dict_play('dictionary.txt')
  else
    referee = HumanPlayer.new
  end

  Hangman.new({guesser: guesser, referee: referee}).play
end
